# portrait2dice

Generate and visualize a dice mosaic using an image as input.

| ![image](lion.jpg) | ![image](lion_half.jpg) | ![image](lion_dice.png) |
|:-:|:-:|:-:|
| Original | Halftone (intermediary) | How dice mosaic will appear |


## Dependencies

This project needs [Halftones](https://gitlab.com/gpds-unb/halftones) package installed.  Additional and required packages: [numpy](https://numpy.org/), [tqdm](https://github.com/tqdm/tqdm), [scipy](https://www.scipy.org/), and [absl](https://abseil.io/docs/python/quickstart).

## Example of usage

```bash
./portrait2dice --input=dice.png --save_halftone=1 \
                --halftone=error_diffusion_floyd_steinberg \
                --face_color=0 --save_map=1 --distance=euclidean \
                --invert_halftone=0 --allow_empty_face=0
```

## License

This project is licensed under [The 2-Clause BSD License](https://opensource.org/licenses/BSD-2-Clause).